import hmacSHA1 from "crypto-js/hmac-sha1";
import Base64 from "crypto-js/enc-base64";
// import DataModel from "./models/data"
import { Data } from "./models/data";
import cryptconf from "./config.json";
// import { Query } from 'mongoose';
import util from "util";
import fs from "fs";
import path from "path";

const dataDirectory = process.env.DATA_DIR || "./files";

// recursive: true makes this not error on existing folders
fs.mkdirSync(dataDirectory, { recursive: true });

/**
 * Current value stores the last result of the encryption
 */
const current_value = cryptconf.start_value;
let counter = 1;
/**
 * Key for the encryption
 */
const key = cryptconf.key;
/**
 * Amounts of data to be tested and stored
 */
const rounds = Number(process.env.BENCH_ROUNDS) || cryptconf.rounds;
/**
 * Time needed for cryptography
 */
let crypt_time_sum = 0;

/**
 * Encrypts the current_value (and substracts the time needed)
 * @returns The current value in Base64s
 */
function crypt(): string {
  const newValue = current_value + counter;
  counter++;
  return newValue;
}

/**
 * Gets a Query to get the possible matching document
 * @param firstField The string for the query
 * @returns The Query to be used.
 */
function getPossibleMatchingDocument(firstField: string) {
  for (const fileName of fs.readdirSync(dataDirectory)) {
    try {
      const fileContents = fs.readFileSync(path.join(dataDirectory, fileName), "utf8");
      const data = JSON.parse(fileContents) as Data;

      if (data.first === firstField) return data;
    } catch (err) {
      console.error("File read error!", err);
    }
  }

  return null;
}

function run() {
  /**
   * Corrupted data counter
   * A corrupted data is a data where one of the fields do not match the expected
   */
  let corrupted = 0;
  /**
   * Valid data counter
   * A valid data is a data where all of the fields match the expected
   */
  let valid = 0;
  /**
   * New data counter
   */
  let new_data = 0;
  /**
   * Start time for calculating speed
   */
  let start_time = new Date();

  for (let i = 0; i < rounds; i++) {
    // Object with the expected fields
    let obj: Data = {
      first: crypt(),
      second: crypt(),
      third: crypt(),
      fourth: crypt(),
    };
    // Since we do not know the IDs, we try to obtain a possible matching object by the first field
    const matchingObject = getPossibleMatchingDocument(obj.first);
    const filePath = path.join(dataDirectory, `${obj.first}.json`);
    // If there is no matching object we save a new one
    if (matchingObject === null) {
      fs.writeFileSync(filePath, JSON.stringify(obj));
      new_data++;
    } else {
      // Check matching fields. Data is corrupted if only one is not matching
      // No need to check first since the object has been obtained by the first field
      if (
        matchingObject.second === obj.second &&
        matchingObject.third === obj.third &&
        matchingObject.fourth === obj.fourth
      ) {
        valid++;
      } else {
        corrupted++;
        // Update the fields with the correct ones
        matchingObject.second = obj.second;
        matchingObject.third = obj.third;
        matchingObject.fourth = obj.fourth;
        fs.writeFileSync(filePath, JSON.stringify(obj));
      }
    }

    if (i % 1000 === 0) {
      const end_time = new Date();
      const time_difference = end_time.getTime() - start_time.getTime() - crypt_time_sum;
      console.log(
        util.inspect(
          {
            valid: valid,
            corrupted: corrupted,
            new: new_data,
            time: time_difference / 1000,
            ops: Math.round((rounds / time_difference) * 100) / 100,
          },
          { compact: true }
        )
      );
    }
  }
  /**
   * End of the workflow
   */
  let end_time = new Date();
  /**
   * Time elapsed during operation (in ms)
   */
  let time_difference = end_time.getTime() - start_time.getTime() - crypt_time_sum;
  console.log("Valid: " + valid);
  console.log("Corrupted: " + corrupted);
  console.log("New: " + new_data);
  console.log("Completion time: " + time_difference + "ms");
  console.log("Ops/sec: " + rounds / time_difference + "ms");
  process.exit(0);
}

console.log("Starting...");
run();
