import mongoose from "mongoose";

mongoose.connect("mongodb://localhost/StorageBenchmark", {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});
export default mongoose;
