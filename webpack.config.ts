import { Configuration } from "webpack";

import CircularDependencyPlugin from 'circular-dependency-plugin';
import path from 'path';

import nodeExternals from "webpack-node-externals";
import NodemonPlugin from "nodemon-webpack-plugin";
import { CleanWebpackPlugin } from "clean-webpack-plugin";

export default {
    target: 'node',

    resolve: {
        extensions: ['.ts', '.js'],
    },

    entry: './src/index.ts',
    devtool: 'source-map',
    mode: 'development',
    output: {
        filename: "index.js",
        path: path.resolve("dist"),
    },
    externals: [
        nodeExternals({
            allowlist: [/lodash-es/, /^@litbase/],
        }),
        nodeExternals({
            allowlist: [/lodash-es/, /^@litbase/],
            modulesDir: path.resolve(__dirname, "node_modules"),
        }),
    ],
    stats: "minimal",

    // Add the loader for .ts files.
    module: {
        rules: [
            {
                test: /\.tsx?|\.jsx$/,
                use: [
                    { loader: "cache-loader" },
                    {
                        loader: "thread-loader",
                        options: {
                            poolRespawn: false,
                        },
                    },
                    {
                        loader: "babel-loader",
                        options: {
                            cacheDirectory: true,
                            envName: "development",
                        },
                    }
                ],
            },
        ],
    },
    plugins: [
        new CleanWebpackPlugin(),
        new NodemonPlugin({
            nodeArgs: ["--trace-deprecation", "--inspect" /*, '--inspect-brk'*/]
        }),
        new CircularDependencyPlugin({
            exclude: /node_modules/,
            failOnError: true
        })
    ]
} as Configuration;
