FROM node:alpine

WORKDIR /opt

COPY . /opt/
RUN yarn install --inline-builds
#RUN yarn build

CMD ["yarn", "ts-node", "/opt/src/index.ts"]
